package top.lishuoboy.graalvm1;

import org.springframework.boot.autoconfigure.SpringBootApplication;
import top.lishuoboy.dependency.sb.sb.MySbUtil;

@SpringBootApplication
public class LishuoboyGraalVm1Application {

    public static void main(String[] args) {
        MySbUtil.run(LishuoboyGraalVm1Application.class, args);
    }

}
