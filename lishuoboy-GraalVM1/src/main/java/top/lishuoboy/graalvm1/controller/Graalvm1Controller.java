package top.lishuoboy.graalvm1.controller;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class Graalvm1Controller {
    @GetMapping("/")
    public Object index() {
        return "hello Graalvm1";
    }
}
